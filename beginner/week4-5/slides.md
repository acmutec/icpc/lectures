# Recursion
Several problems in competitive programming you can see it hard with iterative programming but if you see with recursion paradigm it becomes easy. 
**Def.-** The recursive function is a function that calls itself. 
> A recursive paradigm use a function that calls itself to work on a smaller problem. 
> This is called the recursion step.

![Recursion](https://cdn-images-1.medium.com/max/1600/1*appBwh6_RtvocVxwqpplHA.jpeg)

## Requirements:
- The recursive method should converge
- Each recursive function should have a or more base case(s)

## Why recursion?
1.  Recursive code is often shorter and easier than iterative code.
2.  It's most useful for tasks that can be defined in smaller similar tasks. Example: sort, search, some np problems, etc.

__Example__
```cpp
    int factorial(int num){
        if(num==1)
            return num;
        return num*factorial(num-1);
    }
```
**Other example:**
```cpp
    void specialPrint(int num){
        if(num<0)
            return;
        if(!(num%2))
            cout<<num<<" ";
        specialPrint(num-1);
        if(!(num%2))
            cout<<num<<" ";
    }
```

| Recursion | Iterative |
| ------------------ | ---- |
| Need a base case to finish | Need a condition to finish |
| Each call needs extra space on the stack | Each iteration doesn't need extra space  |
| Some problems are easier to do with recursion | Iterative solutions aren't too easy to find like a recursive solution |
| Some problems haven't got obvious iterative solutions | Some iterative solutions are more efficient than recursive solutions (overhead)|

## Examples
- GCD (Greatest Common Divisor)
- Fibonacci Series, Factorial Finding
- Merge Sort, Quick Sort
- Binary Search
- Tree Traversals and many Tree Problems: InOrder, PreOrder PostOrder
- Graph Traversals: DFS [Depth First Search] and BFS [Breadth First Search]
- Dynamic Programming Examples
- Divide and Conquer Algorithms
- Towers of Hanoi

## **Tower of Hanoi**
![Tower of Hanoi](https://www.codenuclear.com/wp-content/uploads/2017/08/Tower_of_Hanoi.jpg)
**Rules:**
1) Only one disk can be moved at a time.
2) Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack i.e. a disk can only be moved if it is the uppermost disk on a stack.
3) No disk may be placed on top of a smaller disk.
    
**Graphic Solution**
![Solution TowerH](https://www.includehelp.com/data-structure-tutorial/images/tower-of-hanoi-2.png)

### **Algorithm**
 
```cpp
void HanoiFunction(int num,string origen,string destino,string auxiliar){
     if (num == 1){
        cout<<"mueve de "<<origen<<" disco "<<num<<" hacia "<<destino<<endl;
        return;
     }
     else{
            HanoiFunction(num-1,origen,auxiliar,destino);
            cout<<"mueve de "<<origen<<" disco "<<num<<" hacia "<<destino<<endl;
            HanoiFunction(num-1,auxiliar,destino,origen);
    }
}
```

## Euclidean's Algorithm - GCD
```cpp
    int gcd(int a,int b){
        if(b==0)
            return a;
        return gcd(b,a%b);
    }
```
```cpp
    int gcd(int a,int b){
        return b ? gcd(b,a%b):a;
    }
```

## LCM
```cpp
    int lcm(int a,int b){
        return a*b/gcd(a,b);
    }
```

#### **Problem**
https://www.codechef.com/problems/FLOW016   